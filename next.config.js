/** @type {import('next').NextConfig} */
const withPlugins = require('next-compose-plugins')
const optimizedImages = require('next-optimized-images')

const nextConfig = {
  reactStrictMode: true,
  eslint: {
    dirs: ['src'],
  },
  images: {
    disableStaticImages: true,
  }
}

module.exports = withPlugins([
  [optimizedImages, {
    handleImages: ['jpeg', 'png', 'svg', 'webp'],
    responsive: {
      sizes: [320, 640, 960, 1200, 1800, 2400]
    },
    removeOriginalExtension: true
  }],
], nextConfig)

