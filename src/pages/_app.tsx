import { AppProps } from 'next/app'
import React from 'react'

import '../styles/globals.css'

function MyApp({ Component, pageProps }: AppProps) {
  React.useEffect(() => {
    2 + 2
  }, [])
  return <Component {...pageProps} />
}

export default MyApp
